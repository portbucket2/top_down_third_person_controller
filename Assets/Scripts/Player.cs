using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject camera_root;
    public GameObject camera_h;
    public float cam_follow_speed;
    public bool cam_auto_rotate;
    public float cam_rot_speed;

    private Vector3 intended_velocity = Vector3.zero;
    private Vector3 velocity = Vector3.zero;
    public float movement_speed;
    private float intended_rotation = 0;
    public float angular_acceleration = 30;
    public float acceleration = 10;

    public Animator player_mesh_animator;

    private int idle_move_blend = Animator.StringToHash("idle_move");

    public GameObject mesh_root;

    public GameObject joystick_base;
    public GameObject joystick_handle;

    private bool mouse_pressed = false;

    private Vector2 touch_diff = Vector2.zero;
    private float touch_diff_magnitude = 0;
    private Vector2 mousepos = Vector2.zero;
    private Vector2 touch_base = Vector2.zero;

    public bool joystick_follow = false;
    public float joy_follow_speed = 1.5f;
    public float joy_follow_threshold = 0.75f;
    public float joy_max_distance = 49f;
    private float joy_angle = 0;

    private int walk_run_state = 0; //0 walk, 1 run

    private Rigidbody rb;

    public Text debug_text;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        joy_max_distance = Screen.height * joy_max_distance / 1000;

        camera_root.transform.parent = null;
    }

    
    void Update()
    {
        mousepos = Input.mousePosition;

        if (Input.GetMouseButtonDown(0))
        {
            mouse_pressed = true;

            joystick_base.SetActive(true);

            touch_base = mousepos;
        }

        if (Input.GetMouseButtonUp(0))
        {
            mouse_pressed = false;

            joystick_base.SetActive(false);

            touch_diff = Vector2.zero;
            joystick_base.transform.position = Vector2.zero;

            intended_velocity = Vector3.zero;
        }

        if (Input.GetMouseButton(0))
        {
            touch_diff = mousepos - touch_base;
            touch_diff_magnitude = touch_diff.magnitude;

            if (touch_diff.magnitude > 1) //start moving only after dragging a bit
            {
                intended_velocity.x = touch_diff.x;
                intended_velocity.z = touch_diff.y;

                intended_velocity = Quaternion.Euler(0, camera_h.transform.eulerAngles.y, 0) * intended_velocity; //Rotating the velocity vector with camera_h rotation

                intended_rotation = Mathf.Atan2(intended_velocity.x, intended_velocity.z);
            }
        }

        if (touch_diff_magnitude < joy_max_distance)
        {
            joystick_handle.transform.position = mousepos;
        }
        else
        {
            joystick_handle.transform.position = touch_base + touch_diff.normalized * joy_max_distance;
        }

        if (joystick_follow) //joystick base slowly comes towards finger
        {
            if (touch_diff_magnitude > joy_max_distance * joy_follow_threshold)
            {
                touch_base = Vector2.Lerp(touch_base, mousepos, Time.deltaTime * joy_follow_speed);
            }
        }

        joystick_base.transform.position = touch_base;
    }

    private void FixedUpdate()
    {
        mesh_root.transform.rotation = Quaternion.Euler(Vector3.up * intended_rotation * Mathf.Rad2Deg);

        velocity = Vector3.Lerp(velocity, intended_velocity.normalized * movement_speed, Time.fixedDeltaTime * acceleration);

        rb.velocity = velocity;

        camera_root.transform.position = Vector3.Lerp(camera_root.transform.position, transform.position, Time.fixedDeltaTime * cam_follow_speed);
        
        float rb_vel_magnitude = rb.velocity.magnitude;

        player_mesh_animator.SetFloat(idle_move_blend, rb_vel_magnitude / movement_speed);



        if (cam_auto_rotate)
        {
            float angle = Vector3.Angle(mesh_root.transform.forward, camera_h.transform.forward) * Mathf.Deg2Rad;

            float auto_rotate_speed = (Mathf.PI - angle) * rb_vel_magnitude * cam_rot_speed;

            camera_h.transform.rotation = Quaternion.Lerp(camera_h.transform.rotation, mesh_root.transform.rotation, Time.fixedDeltaTime * auto_rotate_speed);
        }

    }
}
